package com.example.apps.qrcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QrcodeApplication {

	public static void main(String[] args) {
		SpringApplication.run(QrcodeApplication.class, args);
	}
private int id;
private String Description;
private String Type;
private String UserDetail;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getDescription() {
	return Description;
}
public void setDescription(String description) {
	Description = description;
}
public String getType() {
	return Type;
}
public void setType(String type) {
	Type = type;
}
public String getUserDetail() {
	return UserDetail;
}
public void setUserDetail(String userDetail) {
	UserDetail = userDetail;
}
 
}


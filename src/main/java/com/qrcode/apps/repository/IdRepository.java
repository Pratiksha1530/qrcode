package com.qrcode.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

public interface IdRepository extends CrudRepository < Id , Long > {

}

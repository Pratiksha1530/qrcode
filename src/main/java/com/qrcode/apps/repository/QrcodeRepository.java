package com.qrcode.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.qrcode.apps.model.Qrcode;

public interface QrcodeRepository extends CrudRepository <Qrcode , Long> {

}

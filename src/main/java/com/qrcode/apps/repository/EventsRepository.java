package com.qrcode.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.qrcode.apps.model.Events;


public interface EventsRepository extends CrudRepository<Events , Long >{

}

package com.qrcode.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.qrcode.apps.model.Location;

public interface LocationRepository extends CrudRepository <Location , Long >{

}

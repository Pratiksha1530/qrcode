package com.qrcode.apps.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity

public class Events {
	
	@Id
	Long Event_ID;
	String EventName;
	String Description;
	public Long getEvent_ID() {
		return Event_ID;
	}
	public void setEvent_ID(Long event_ID) {
		Event_ID = event_ID;
	}
	public String getEventName() {
		return EventName;
	}
	public void setEventName(String eventName) {
		EventName = eventName;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	
	

}

package com.qrcode.apps.service;

import org.springframework.beans.factory.annotation.Autowired;


import com.qrcode.apps.model.Events;
import com.qrcode.apps.repository.EventsRepository;

public class EventsServiceImpl implements EventsService {
	
	@Autowired
	EventsRepository eventRepository;
	
	@Override
	public void add(Events events) {
		eventRepository.save(events);

	}

}
